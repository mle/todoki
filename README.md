![todoki-logo with dark font](https://gitlab.com/mle/todoki/-/raw/main/assets/todoki-dark.png)

## Manage your todos

Todoki was inspired by the catch up feature from Slack to swipe through messages as a way to review and triage which items to keep and which one to be marked as done.

> I was able to take 250 todos down to 55 todos in 10 minutes.

The name todoki is inspired by a combination of "todos" with GitLab's mascot "tanuki".

```
todo + tanuki = todoki
```

🕹️ [Try it now with the latest prototype!](https://preview.flutterflow.app/todoki-t01370/oByCyjz2cSbPt3RRTlH6) (Best viewed on mobile device)

Built by [Michael Le](https://gitlab.com/mle) using [Flutterflow](https://flutterflow.io/).

## Demo

[![demo video](assets/swipe-scene.jpg)](/assets/swipe-demo.mp4)


## Setup

A personal access token is used for GitLab API.

Go to the  [Personal Access Tokens](https://gitlab.com/-/user_settings/personal_access_tokens) in GitLab, then follow the instructions to create a token.

- Enter a token name
- Mark the `read_api` and `api` checkbox 
- Click "Create personal access token". 

On the following screen, copy the newly created access token and paste it into the access token input above.

### For self-managed instances

The default setting is `gitlab.com`. For self-managed instances  enter the URL specific to your instance.

## Feedback

Feel free to [fill out a form](https://tally.so/r/woA57V) or create a [new issue](https://gitlab.com/mle/todoki/-/issues) for any issues or ideas for improvements.

